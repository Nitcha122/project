﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Globalization;

public class Player : MonoBehaviour
{
    public Text YouWin;
    public float movement;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        manageInput();
    }
    public void manageInput()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(-movement * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(movement * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0, movement * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(0, -movement * Time.deltaTime, 0);
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            YouWin.enabled = true;
        }
    }
}
