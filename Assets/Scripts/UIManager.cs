﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using System.Globalization;

public class UIManager : MonoBehaviour
{
    public Button reset;
    public Text youLose;
    public Text clock;
    public bool StartTimer;
    public float timer;
    private static UIManager instance;

    public static UIManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<UIManager>();
                if (instance == null)
                {
                    GameObject singleton = new GameObject();
                    singleton.AddComponent<UIManager>();
                    singleton.name = "(Singleton) UIManager";
                }
            }
            return instance;
        }
    }
    private void Awake()
    {
        instance = this;
    }
    public void Update()
    {
        if (StartTimer)
        {
            ManageTimer();
        }
        clock.text = ((int)timer).ToString();
    }
    void ManageTimer()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = 0;
            GameOver();
        }
    }
    public void TimeStart()
    {
        StartTimer = true;
    }
    public void GameOver()
    {
        youLose.enabled = true;
        reset.enabled = true;
    }
}
