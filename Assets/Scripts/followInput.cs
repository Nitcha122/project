﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class followInput : MonoBehaviour
{
    public float distanceVertical;
    public float distanceHorizontal;
    
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        manageInput();
    }
    public void manageInput()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.DOMoveX(distanceHorizontal, 1f);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.DOMoveX(-distanceHorizontal, 1f);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.DOMoveY(-distanceVertical, 1f);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.DOMoveY(distanceVertical, 1f);
        }
    }
}
